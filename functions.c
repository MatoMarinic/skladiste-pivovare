#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"


void porukaIzbornik(const char* poruka)
{
	system("cls");
	printf("\n\t\t============================");
	printf("\n\t\t#    Skladiste pivovare	   #");
	printf("\n\t\t============================");
}

void dodavanjeInformacija()
{
	PIVO dodavanje = { 0 };
	FILE* fp = NULL;
	fp = fopen("skladiste.bin", "ab+");
	if (fp == NULL)
	{
		perror("\n\t\tOtvaranje");
		exit(1);
	}
	porukaIzbornik("DODAVANJE INFORMACIJA");
	printf("\n\n\t\tDODAVANJE INFORMACIJA");
	printf("\n\n\t\tUnesite informacije:");
	printf("\n");
	printf("\n\t\tID piva = ");
	scanf("%u", &dodavanje.idPiva);
	
	printf("\n\t\tIme piva: ");
	scanf("%29s", dodavanje.imePiva);
		
	
	printf("\n\t\tOkus piva: ");
	scanf("%29s", dodavanje.okusPiva);
		
		
	printf("\n\t\tCijena = ");
	scanf("%f", &dodavanje.cijena);


	printf("\n\t\tUnesite datum isteka roka u formatu (dan/mjesec/godina): ");
	scanf("%d/%d/%d", &dodavanje.istekRokaPiva.dd, &dodavanje.istekRokaPiva.mm, &dodavanje.istekRokaPiva.yyyy);
	
	fwrite(&dodavanje, sizeof(dodavanje), 1, fp);  //dodavanje u strukturu?
	printf("\n\t\tInformacije dodane u datoteku");
	printf("\n\n\t\tPritisnite da bi ste se vratili u glavni izbornik....");
	fclose(fp);
	getch();
	return;
}


void selection_sort_cijena_s(PIVO* info, int brojPiva) {
	PIVO pom;
	int max;
	for (int i = 0; i < brojPiva - 1; i++) {
		max = i;
		for (int j = i + 1; j < brojPiva; j++) {
			if ((info + j)->cijena > (info + max)->cijena) {
				max = j;
			}
		}
		pom.idPiva = (info + i)->idPiva;
		(info + i)->idPiva = (info + max)->idPiva;
		(info + max)->idPiva = pom.idPiva;
		
		strcpy(pom.imePiva, (info + i)->imePiva);
		strcpy((info + i)->imePiva, (info + max)->imePiva);
		strcpy((info + max)->imePiva, pom.imePiva);
	
		strcpy(pom.okusPiva, (info + i)->okusPiva);
		strcpy((info + i)->okusPiva, (info + max)->okusPiva);
		strcpy((info + max)->okusPiva, pom.okusPiva);
		
		pom.cijena = (info + i)->cijena;
		(info + i)->cijena = (info + max)->cijena;
		(info + max)->cijena = pom.cijena;
		
		pom.istekRokaPiva = (info + i)->istekRokaPiva;
		(info + i)->istekRokaPiva = (info + max)->istekRokaPiva;
		(info + max)->istekRokaPiva = pom.istekRokaPiva;
	}

	return;
}

void selection_sort_cijena_u(PIVO* info, int brojPiva) {
	PIVO pom;
	int max;
	for (int i = 0; i < brojPiva - 1; i++) {
		max = i;
		for (int j = i + 1; j < brojPiva; j++) {
			if ((info + j)->cijena < (info + max)->cijena) {
				max = j;
			}
		}
		pom.idPiva = (info + i)->idPiva;
		(info + i)->idPiva = (info + max)->idPiva;
		(info + max)->idPiva = pom.idPiva;
	
		strcpy(pom.imePiva, (info + i)->imePiva);
		strcpy((info + i)->imePiva, (info + max)->imePiva);
		strcpy((info + max)->imePiva, pom.imePiva);
	
		strcpy(pom.okusPiva, (info + i)->okusPiva);
		strcpy((info + i)->okusPiva, (info + max)->okusPiva);
		strcpy((info + max)->okusPiva, pom.okusPiva);
		
		pom.cijena = (info + i)->cijena;
		(info + i)->cijena = (info + max)->cijena;
		(info + max)->cijena = pom.cijena;
	
		pom.istekRokaPiva = (info + i)->istekRokaPiva;
		(info + i)->istekRokaPiva = (info + max)->istekRokaPiva;
		(info + max)->istekRokaPiva = pom.istekRokaPiva;
	}

	return;
}

void sortiranje(PIVO* informacije, int brojPiva) {
	char odabir2, odabir3;
	char izabrano[20];
	system("cls");
	porukaIzbornik("SORTIRANJE INFORMACIJA");
	printf("\n\n\t\tSORTIRANJE INFORMACIJA");
	printf("\n\n\t\tOdaberite kriterij sortiranja:");
	printf("\n\n\t\t1.Cijena");
	do {
		odabir2 = getch();
		if (odabir2 < '1' || odabir2 > '2') {
			printf("\n\t\tPogresan odabir! Pokusajte ponovo: ");
		}
	} while (odabir2 < '1' || odabir2 > '2');
	printf("\n\n\t\tRedosljed:\n\n\t\t1. Uzlazno\n\n\t\t2. Sizalno\n");
	do {
		odabir3 = getch();
		if (odabir3 < '1' || odabir3 > '2') {
			printf("\n\t\tPogresan odabir! Pokusajte ponovo: ");
		}
	} while (odabir3 < '1' || odabir3 > '2');

	switch (odabir2) {

	case '1': 
		if (odabir3 == '1') {
		selection_sort_cijena_u(informacije, brojPiva);
		strcpy(izabrano, "Cijena - uzlazno");
		}

	else {
		selection_sort_cijena_s(informacije, brojPiva);
		strcpy(izabrano, "Cijena - silazno");

		}
	break;
	}
	system("cls");
	printf("\n\t\tIzabran sort: %s\n", izabrano);
	printf("\n\t\tPodaci sortirani\n");
	printf("\n\n\t\tPritisnite da bi se vratili na pocetak....");
	getch();
	return;
}

void sortiranjeIzbornik() {
	int brojPiva;
	PIVO* informacije = NULL;
	FILE* fp = NULL;
	
	fp = fopen("skladiste.bin", "rb");
	if (fp == NULL) {
		perror("\n\tCitanje!");
		exit(1);
	}
	else {
		fread(&brojPiva, sizeof(int), 1, fp);
		informacije = (PIVO*)calloc(brojPiva, sizeof(PIVO));
		if (informacije == NULL) {
			printf("\n\tGreska pri zauzimanju memorije!");
		}
		else {
			fread(informacije, sizeof(PIVO), brojPiva, fp);
			fclose(fp);
			sortiranje(informacije, brojPiva);
			free(informacije);
		}
	}
	return;
}


void uredjivanjeInformacija(){

	PIVO uredjivanje = { 0 };
	int pronadjen = 0;

	char imePiva[30] = { '\0' };

	FILE* fp = NULL;
	FILE* temp = NULL;

	fp = fopen("skladiste.bin", "rb");
	
	if (fp == NULL) {
		perror("\n\tCitanje");
		exit(1);
	}
	temp = fopen("temp.bin", "ab+");

	if (temp == NULL) {
		perror("\n\tIzmjena");
		exit(1);
	}
	porukaIzbornik("Uredjivanje");
	printf("\n\n\n\t\tUREDJIVANJE INFORMACIJA");

	printf("\n\n\n\t\tUnesite ime piva kojeg zelite izmjeniti: ");
	scanf("%29s", imePiva);

	while (fread(&uredjivanje, sizeof(uredjivanje), 1, fp) == 1) {  
		if (strcmp(uredjivanje.imePiva, imePiva) == 0)             
		{
			pronadjen = 1;
			printf("\n\t\tUnesite novi ID piva: ");
			scanf("%u", &uredjivanje.idPiva);
			

			printf("\n\t\tUnesite novo ime piva: ");
			scanf("%29s", uredjivanje.imePiva);
		

			printf("\n\t\tUnesite novi okus piva: ");
			scanf("%29s", uredjivanje.okusPiva);
		

			printf("\n\t\tUnesite novu cijenu piva: ");
			scanf("%f", &uredjivanje.cijena);
			

			printf("\n\t\tUnesite novi istek roka piva u formatu (dan/mjesec/godina): ");
			scanf("%d/%d/%d", &uredjivanje.istekRokaPiva.dd, &uredjivanje.istekRokaPiva.mm, &uredjivanje.istekRokaPiva.yyyy);
			
			printf("\n\n\t\tInformacije uspjesno izmjenjene!");
			printf("\n\n\t\tPritisnite da bi se vratili na pocetak....");
		}
		fwrite(&uredjivanje, sizeof(uredjivanje), 1, temp);
	}
	if (pronadjen == 0)
	{
		printf("\n\n\t\tInformacija nije pronadjena\n");
	}
	fclose(fp);
	fclose(temp);
	remove("skladiste.bin");
	rename("temp.bin", "skladiste.bin");
	getch();
	return;
}

void pretrazivanje()
{
	char izbor[3] = { '\0' };
	char pretraga[30] = { '\0' };
	int pronadjen = 0;
	PIVO dodavanje = { 0 };
	FILE* fp = NULL;
	fp = fopen("skladiste.bin", "rb");
	if (fp == NULL)
	{
		perror("\n\t\tCitanje");
		exit(1);
	}
	porukaIzbornik("Pretrazivanje informacija");
	printf("\n\n\t\tPRETRAZIVANJE INFORMACIJA");

	printf("\n\n\t\tUnesite ime piva kojeg zelite pretraziti: ");
	scanf("%29s", pretraga);

	while (fread(&dodavanje, sizeof(dodavanje), 1, fp))
	{
		if (strcmp(dodavanje.imePiva, pretraga) == 0)
		{
			pronadjen = 1;
			break;
		}
	}

	if (pronadjen)
	{
		printf("\n\t\tInformacija o pivu pronadjena:");
		printf("\n\t\tID piva = %u\n", dodavanje.idPiva);
		printf("\t\tIme piva = %s\n", dodavanje.imePiva);
		printf("\t\tOkus piva = %s\n", dodavanje.okusPiva);
		printf("\t\tCijena piva = %.2f\n", dodavanje.cijena);
		printf("\t\tDatum isteka roka(dan/mjesec/godina) =  (%d/%d/%d)\n", dodavanje.istekRokaPiva.dd, dodavanje.istekRokaPiva.mm, dodavanje.istekRokaPiva.yyyy);
		printf("\n\n\n\t\tPritisnite da bi se vratili na pocetak....");
	}
	else
	{
		printf("\n\n\t\tInformacija o pivu nije pronadjena! Zelite li nastaviti pretrazivanje: ");
		scanf("%2s",izbor);
		if (strcmp(izbor, "da") == 0 || strcmp(izbor, "Da") == 0 || strcmp(izbor, "DA") == 0 || strcmp(izbor, "dA") == 0) {		//usporeivanje stringa 
			pretrazivanje();
		}
		else if (strcmp(izbor, "ne") == 0 || strcmp(izbor, "Ne") == 0 || strcmp(izbor, "NE") == 0 || strcmp(izbor, "nE") == 0) {      //usporeivanje stringa
			izbornik();
		}
	}
	fclose(fp);
	getch();
}


int ispisInformacija()
{
	PIVO dodavanje = { 0 };
	FILE* fp = NULL;
	unsigned int brojac = 1;
	int pronadjen = 0;
	porukaIzbornik("ISPIS INFORMACIJA");
	printf("\n\n\t\tISPIS INFORMACIJA");
	fp = fopen("skladiste.bin", "rb");
	if (fp == NULL)
	{
		perror("\n\t\tCitanje\n");
		exit(1);
	}
	
	while (fread(&dodavanje, sizeof(dodavanje), 1, fp))
	{
		printf("\n\n\t\t%d. informacija\n\n", brojac);
		printf("\t\tID piva = %u\n", dodavanje.idPiva);
		printf("\t\tIme piva = %s\n", dodavanje.imePiva);
		printf("\t\tOkus piva = %s\n", dodavanje.okusPiva);
		printf("\t\tCijena piva = %.2f\n", dodavanje.cijena);
		printf("\t\tDatum isteka roka(dan/mjesec/godina) =  (%d/%d/%d)\n", dodavanje.istekRokaPiva.dd, dodavanje.istekRokaPiva.mm, dodavanje.istekRokaPiva.yyyy);
		pronadjen = 1;
		brojac++;
	}
	if (!pronadjen)
	{
		printf("\n\n\t\tNema zapisa");
	}
	fclose(fp);
	printf("\n\n\t\tPritisnite da bi ste se vratili u glavni izbornik....");
	getch();
	return brojac;
}

void brisanjeInformacija()
{
	int pronadjen = 0;
	int brisanjeID = 0;
	PIVO dodavanje = { 0 };
	FILE* fp = NULL;
	FILE* temp = NULL;
	porukaIzbornik("BRISANJE INFORMACIJA");
	printf("\n\n\t\tBRISANJE INFORMACIJA");
	fp = fopen("skladiste.bin", "rb");
	if (fp == NULL)
	{
		perror("\n\t\tCitanje\n");
		exit(1);
	}
	temp = fopen("temp.bin", "wb");

	if (temp == NULL)
	{
		fclose(fp);
		perror("\n\t\tOtvaranje\n");
		exit(1);
	}

	printf("\n\n\t\tUnesite ID piva koji zelite obrisati: ");
	scanf("%d", &brisanjeID);

	while (fread(&dodavanje, sizeof(dodavanje), 1, fp))
	{

		if (dodavanje.idPiva != brisanjeID)
		{
			fwrite(&dodavanje, sizeof(dodavanje), 1, temp);
		}
		else
		{
			pronadjen = 1;
		}
	}
	if (pronadjen) {
		printf("\n\t\tInformacija o pivu uspjesno obrisana.....");
	}
	else {
		printf("\n\t\tInformacija o pivu nije pronadjena.....");
	}
	fclose(fp);
	fclose(temp);
	remove("skladiste.bin");
	rename("temp.bin", "skladiste.bin");
	getch();
	return;
}

void izlazak(void) {
	porukaIzbornik("IZLAZAK");
	char izbor[3] = { '\0' };
	printf("\n\n\t\tIZLAZAK IZ PROGRAMA");
	printf("\n\n\t\tJeste li sigurni da zelite izaci iz programa? ");
	scanf("%2s", izbor);
	
	if (strcmp(izbor, "da") == 0 || strcmp(izbor, "Da") == 0 || strcmp(izbor, "DA") == 0 || strcmp(izbor, "dA") == 0) {		//usporeivanje stringa 
		printf("\n\n\n\t\tUspjesno ste izasli iz programa...\n\n");
		exit(EXIT_SUCCESS);
	}
	else if (strcmp(izbor, "ne") == 0 || strcmp(izbor, "Ne") == 0 || strcmp(izbor, "NE") == 0 || strcmp(izbor, "nE") == 0) {      //usporeivanje stringa
		izbornik();
	}
	return;
}

void izbornik()
{
	int brojPiva = 0;
	int izbor = 0;
	do
	{
		porukaIzbornik("GLAVNI IZBORNIK");
		printf("\n\n\n\n\t\tGLAVNI IZBORNIK");
		printf("\n\n\n\t\t1. Dodavanje informacija u datoteku");
		printf("\n\t\t2. Ispis informacija iz datoteke");
		printf("\n\t\t3. Pretrazivanje informacija");
		printf("\n\t\t4. Sortiranje informacija");
		printf("\n\t\t5. Uredjivanje informacija");
		printf("\n\t\t6. Brisanje informacija");
		printf("\n\t\t0. Izlazak iz programa");
		printf("\n\n\n\t\tOdaberite broj - ");
		scanf("%d", &izbor);
		switch (izbor)
		{
		case 1:
			dodavanjeInformacija();
			break;

		case 2:
			brojPiva = ispisInformacija();
			break;

		case 3:
			pretrazivanje();
			break;

		case 4:
			sortiranjeIzbornik();
			break;

		case 5:
			uredjivanjeInformacija();
			break;

		case 6:
			brisanjeInformacija();
			break;

		case 0:
			izlazak();
			break;

		default:
			printf("\n\n\n\t\tPogresan unos. Pokusajte ponovno");
			getch();
		}
	} while (izbor != 0);
}