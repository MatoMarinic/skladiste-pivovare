#ifndef HEADER_H
#define HEADER_H


typedef struct datum
{
	int yyyy;
	int mm;
	int dd;
} DATUM;

typedef struct pivovara
{
	unsigned int idPiva;
	float cijena;
	char imePiva[30];
	char okusPiva[30];
	DATUM istekRokaPiva;
} PIVO;

void porukaIzbornik(const char*);
void dodavanjeInformacija();
void sortirajPodatke_manje_vece(PIVO*, int);
void sortiranje(int);
void pretrazivanje();
int ispisInformacija();
void brisanjeInformacija();
void izlazak(void);
void izbornik();

#endif // !HEADER_H
